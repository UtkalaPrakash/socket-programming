all: client server

client:
	gcc -o client client.c -lnsl -lsocket -lresolv

server:
	gcc -o allServers servers.c -lnsl -lsocket -lresolv

servers:
	./allServers

clean:
	rm -f allServers client
