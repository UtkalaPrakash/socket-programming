#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<netdb.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<sys/wait.h>
#include <sys/stat.h>

struct sockaddr_in ser_addr = {.sin_family = AF_INET, .sin_port = htons(25834)};

int main (int argc, char **argv) {
	char func_name[4], *file_name = "num.csv";
	if ( argc != 2 ) {
		fprintf(stderr, "Improper usage\nUsage: ./client <min|max|sum|sos>\n");
		exit(0);
	}
	if ( strcmp(*(argv+1), "min") != 0 && strcmp(*(argv+1), "max") != 0 && strcmp(*(argv+1), "sum") && strcmp(*(argv+1), "sos") != 0 ) {
		fprintf(stderr, "Improper function name %s\nUsage: ./client <min|max|sum|sos> file_name\n", *(argv+1));
		exit(0);
	}
	strcpy(func_name, *(argv+1));
	if ( access(file_name, F_OK) == -1 ) {
		fprintf(stderr, "Input file %s doesn't exist\n", *(argv+2));
		exit(0);
	}
	FILE *file_handler;
	struct stat file_check;
	stat(file_name, &file_check);
	if ( S_ISDIR(file_check.st_mode) ) {
		fprintf(stderr, "Input file %s is a directory\n", *(argv+2));
		exit(0);
	}
	if ( access(file_name, R_OK) == -1 ) {
		fprintf(stderr, "Cannot read input file %s, permission denied\n", *(argv+2));
		exit(0);
	}
	file_handler = fopen(file_name, "r");
	if ( file_handler == NULL) {
		fprintf(stderr, "Input file %s cannot be open\n", *(argv+2));
		exit(0);
	}

	// Send function name and file data via TCP connection
	ser_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	socklen_t len = sizeof(ser_addr);
	int socket_handler = socket(PF_INET, SOCK_STREAM, 0);
	if (socket_handler < 0 ) {
		perror("Error creating the socket");
		exit(1);
	}
	while ( 1 ) {
		int connection = connect(socket_handler, (struct sockaddr*) &ser_addr, len);
		if ( connection == 0 ) break;
	}
	printf("Send function name %s\n", func_name);
	int connection = send(socket_handler, func_name, 4, 0);
	if ( connection < 0 ) {
		perror("Error sending the function name");
		exit(0);
	}
	printf("The client has sent the reduction type %s to AWS\n", func_name);

	int size = 0, i;
	char buffer[11];
	while ( fgets(buffer, 11, file_handler) ) {
		int result = send(socket_handler, buffer, 11, 0);
		if (result < 0 ) {
			perror("Error sending number");
			exit(1);
		}
		size++;
	}
	size--;
	int result = send(socket_handler, "EOT", 4, 0);
	printf("The client has sent %d numbers to AWS\n", size);

	char output[25];
	if ((result = recv(socket_handler, output, 25, 0)) < 0) {
		perror("Error recieving output");
		exit(1);
	}
	printf("The client has received reduction %s: %s\n", func_name, output);
	close(socket_handler);
	return(0);
}
