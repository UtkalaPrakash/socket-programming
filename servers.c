#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<netdb.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<sys/wait.h>
#include <pthread.h>

#define locahost "127.0.0.1"
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

struct sockaddr_in UDPser_addr = {.sin_family = AF_INET, .sin_port = htons(24834)};
struct sockaddr_in TCPser_addr = {.sin_family = AF_INET, .sin_port = htons(25834)};
struct sockaddr_in ser_addr1 = {.sin_family = AF_INET, .sin_port = htons(21834)};
struct sockaddr_in ser_addr2 = {.sin_family = AF_INET, .sin_port = htons(22834)};
struct sockaddr_in ser_addr3 = {.sin_family = AF_INET, .sin_port = htons(23834)};

static
long int min (long int num1, long int num2) {
	int ret = (num1 < num2) ? (num1) : (num2);
	return ret;
}

static
long int max (long int num1, long int num2) {
	int ret = (num1 > num2) ? (num1) : (num2);
	return ret;
}

static
void Output(char *serverName, char *message) {
	pthread_mutex_lock(&m);
	printf("Server %s\t: The %s %s\n", serverName, serverName, message);
	pthread_mutex_unlock(&m);
}

static
void server (char *serverName, struct sockaddr_in backened_addr, int port) {
	char outputStr[1024] = "\0";
	sprintf(outputStr, "is up and running using UDP on port %d", port);
	Output(serverName, outputStr);
	socklen_t length = sizeof(UDPser_addr), len = sizeof(backened_addr);

	// Open UDP connection and listen to data
	int sd = socket(PF_INET, SOCK_DGRAM, 0);
	if (sd < 0 ) {
		perror("socket error");
                exit(1);
        }

	int connection = bind(sd, (struct sockaddr*) &backened_addr, len);
	if ( connection < 0) {
		perror("socket error");
		exit(1);
	}

	// Get function name and data from AWS using UDP connection
	char msg[11], funcName[4], ret[25];
	long int number[1000];
	int size = 0, i;
	recvfrom(sd, funcName, 4, 0, NULL, NULL);
	do {
		msg[0] = '\0';
		recvfrom(sd, msg, 11, 0, NULL, NULL);
		if (strlen(msg) == 0 || strcmp(msg, "EOT") == 0) continue;
		number[size] = atoi(msg);
		size++;
	} while (strcmp(msg, "EOT") != 0);
	sprintf(outputStr, "has recieved %d numbers", size);
	Output(serverName, outputStr);

	if ( strcmp(funcName, "min") == 0 ) {
		long int result = number[0];
		for ( i=0; i<size; i++ ) result = min(result, number[i]);
		sprintf(ret, "%ld", result);
	} else if ( strcmp(funcName, "max") == 0 ) {
		long int result = number[0];
		for ( i=0; i<size; i++ ) result = max(result, number[i]);
		sprintf(ret, "%ld", result);
	} else if ( strcmp(funcName, "sum") == 0 ) {
		long long int result = 0;
		for ( i=0; i<size; i++ ) result += number[i];
		sprintf(ret, "%lld", result);
	} else {
		unsigned long long int result = 0;
		for ( i=0; i<size; i++ ) result += (number[i] * number[i]);
		sprintf(ret, "%llu", result);
	}
	sprintf(outputStr, "has successfully finished reduction %s: %s", funcName, ret);
	Output(serverName, outputStr);
	sprintf(ret, "%s%s", ret, serverName);
	sendto(sd, ret, 25, 0, (struct sockaddr*) &UDPser_addr, length);
	Output(serverName, "has successfully finished sending the reduction value to AWS server");
	close(sd);
}

int AWS(int TCPport, int UDPport) {
	char outputStr[1024] = "\0";
	Output("AWS", "is up and running");
	socklen_t TCPlength = sizeof(TCPser_addr), UDPlength = sizeof(UDPser_addr);
	int i = 0, true = 1;
	char func_name[4];

	// Open UDP connection
	int UDPsocket_handler = socket(PF_INET, SOCK_DGRAM, 0);
	int UDPconnection = bind(UDPsocket_handler, (struct sockaddr*) &UDPser_addr, UDPlength);
        if ( UDPconnection < 0) {
                perror("Failed binding the UDP socket");
		close(UDPsocket_handler);
                exit(1);
        }

	// Open TCP connection and listen from the client
	int TCPsocket_handler = socket(PF_INET, SOCK_STREAM, 0);
	setsockopt(TCPsocket_handler, SOL_SOCKET, SO_REUSEADDR, &true, sizeof(int));
	int result = bind(TCPsocket_handler, (struct sockaddr*) &TCPser_addr, TCPlength);
        if ( result < 0) {
                perror("Failed binding the TCP socket");
                exit(1);
        }
        if ((result = listen(TCPsocket_handler, 1))< 0) {
                perror("Failed listening the TCP socket");
                exit(1);
        }
	int TCPconnection = accept(TCPsocket_handler, NULL, NULL);
	if ( TCPconnection < 0 ) {
		perror("Failed accepted connection");
		exit(1);
	}
	if ((result = recv(TCPconnection, func_name, 4, 0)) < 0 ) {
		perror("Failed recieving the TCP socket");
		close(TCPconnection);
		exit(1);
	}
	result = sendto(UDPsocket_handler, func_name, 4, 0, (struct sockaddr*) &ser_addr1, UDPlength);
	result = sendto(UDPsocket_handler, func_name, 4, 0, (struct sockaddr*) &ser_addr2, UDPlength);
	result = sendto(UDPsocket_handler, func_name, 4, 0, (struct sockaddr*) &ser_addr3, UDPlength);

	char num[11];
	i = 0;
	pthread_mutex_lock(&m);
	while ( strcmp(num, "EOT") != 0 ) {
		num[0] = '\0';
		if ((result = recv(TCPconnection, num, 11, 0)) > 0 ) {
			if ( strcmp(num, "EOT") != 0 && num[0] != '0' && atoi(num) == 0 ) continue;
			if ( strcmp(num, "EOT") == 0 || (i%3) == 0 ) result = sendto(UDPsocket_handler, num, 11, 0, (struct sockaddr*) &ser_addr1, UDPlength);
			if ( strcmp(num, "EOT") == 0 || (i%3) == 1 ) result = sendto(UDPsocket_handler, num, 11, 0, (struct sockaddr*) &ser_addr2, UDPlength);
			if ( strcmp(num, "EOT") == 0 || (i%3) == 2 ) result = sendto(UDPsocket_handler, num, 11, 0, (struct sockaddr*) &ser_addr3, UDPlength);
			i++;
		}
	}
	i--;

	printf("AWS\t: The AWS has received %d numbers from the client using TCP over port %d\n", i, TCPport);
	printf("AWS\t: The AWS sent %d numbers to BackendServer %c\n", (i/3), 'A');
	printf("AWS\t: The AWS sent %d numbers to BackendServer %c\n", (i/3), 'B');
	printf("AWS\t: The AWS sent %d numbers to BackendServer %c\n", (i/3), 'C');
	pthread_mutex_unlock(&m);

	char ind_output[3][25];
	for ( i=0; i<3; i++ ) {
		result = recvfrom(UDPsocket_handler, ind_output[i], 25, 0, NULL, NULL);
		if (result < 0) perror("error recieving file");
		int outLen = strlen(ind_output[i]);
		char ser = ind_output[i][outLen-1];
		ind_output[i][outLen-1] = '\0';
		sprintf(outputStr, "The AWS received reduction result of %s from BackendServer %c using UDP over port %d and it is %s", func_name, ser, UDPport, ind_output[i]);
		Output("AWS", outputStr);
	}
	close(UDPsocket_handler);

	char final_output[25] = "";
	if ( strcmp(func_name, "min") == 0 ) {
		long int finOutput = min((long int) atoi(ind_output[0]), (long int) atoi(ind_output[1]));
		finOutput = min(finOutput, atoi(ind_output[2]));
		sprintf(final_output, "%ld", finOutput);
	} else if ( strcmp(func_name, "max") == 0 ) {
		long int finOutput = max((long int) atoi(ind_output[0]), (long int) atoi(ind_output[1]));
		finOutput = max(finOutput, (long int) atoi(ind_output[2]));
		sprintf(final_output, "%ld", finOutput);
	} else if ( strcmp(func_name, "sum") == 0 ) {
		long long int finOutput = atoi(ind_output[0]) + atoi(ind_output[1]) + atoi(ind_output[2]);
		sprintf(final_output, "%lld", finOutput);
	} else {
		unsigned long long int finOutput = atoi(ind_output[0]) + atoi(ind_output[1]) + atoi(ind_output[2]);
		sprintf(final_output, "%llu", finOutput);
	}

	sprintf(outputStr, "has successfully finished the reduction %s: %s", func_name, final_output);
	Output("AWS", outputStr);
	if((result = send(TCPconnection, final_output, 25, 0)) < 0) {
		perror("Failed sending output");
		close(TCPsocket_handler);
		exit(0);
	}
	Output("AWS", "has successfully finished sending the reduction value to client.");
	close(TCPconnection);
	close(TCPsocket_handler);
	return 0;
}

int main() {
	int i;
	UDPser_addr.sin_addr.s_addr = inet_addr(locahost);
	TCPser_addr.sin_addr.s_addr = inet_addr(locahost);
	ser_addr1.sin_addr.s_addr = inet_addr(locahost);
	ser_addr2.sin_addr.s_addr = inet_addr(locahost);
	ser_addr3.sin_addr.s_addr = inet_addr(locahost);

	while (1) {
		for ( i=0; i<4; i++ ) {
			pid_t pid = fork();
			if ( pid == 0 ) {
				if ( i == 0 ) AWS(25834, 24834);
				if ( i == 1 ) server("A", ser_addr1, 21834);
				if ( i == 2 ) server("B", ser_addr2, 22834);
				if ( i == 3 ) server("C", ser_addr3, 23834);
				exit(0);
			}
		}
		for ( i=0 ; i<4; i++) wait(NULL);
	}
	return(0);
}
